from CMS import views
from django.urls import path

app_name='CMS'

urlpatterns = [
    path('', views.index, name="index"),
    path('change_language/<str:language_code>', views.change_language, name="change_language"),
    path('send_message', views.send_message, name="send_message"),
    path('load_privacity', views.load_privacity, name="load_privacity"),
]