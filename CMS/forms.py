from django import forms
from .models import ContactMe
from django.utils.translation import ugettext_lazy as _

class ContactMeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContactMeForm, self).__init__(*args, **kwargs)
        self.fields['full_name'].widget.attrs['placeholder'] = _("Nombre y apellidos *")
        self.fields['subject'].widget.attrs['placeholder'] = _("Asunto *")
        self.fields['email'].widget.attrs['placeholder'] = _("Email: ejemplo@gmail.com *")
        self.fields['message'].widget.attrs['placeholder'] = _("Mensaje *")

        self.fields['full_name'].widget.attrs['class'] = "form-control"
        self.fields['subject'].widget.attrs['class'] = "form-control"
        self.fields['email'].widget.attrs['class'] = "form-control"
        self.fields['message'].widget.attrs['class'] = "form-control"

    class Meta:
        model = ContactMe
        fields = ['full_name', 'subject', 'email', 'message']



