from django.contrib import admin

from .models import *

from translations.admin import TranslatableAdmin, TranslationInline
from solo.admin import SingletonModelAdmin


# Register your models here.
class ConfigAdmin(TranslatableAdmin, SingletonModelAdmin):
    fieldsets = (
            (u"General data", {
                'fields': ('name', 'photo', 'job_title', ('phone_1', "email"), 'address', 'description', 'favicon', )
            }),

        )
    inlines = [TranslationInline,]

class SkillInfoAdmin(TranslatableAdmin):
    list_display = ('name_skill', 'percentage_skill', 'code_ico',)
    list_display_links = ('name_skill',)
    list_editable = ('percentage_skill',)

    fieldsets = (
            (u"Work Experience", {
                'fields': ('name_skill', 'percentage_skill', 'code_ico',)
            }),
        )
    inlines = [TranslationInline,]

class WorkExperienceInfoAdmin(TranslatableAdmin):
    list_display = ('date_init', 'date_end', 'name', 'job', 'place',)
    list_display_links = ('name',)

    fieldsets = (
            (u"Work Experience", {
                'fields': ('date_init', 'date_end', 'name', 'job', 'place',)
            }),
        )
    inlines = [TranslationInline,]

class EducationInfoAdmin(TranslatableAdmin):
    list_display = ('date_init', 'date_end', 'course_name', 'place',)
    list_display_links = ('course_name',)

    fieldsets = (
            (u"Work Experience", {
                'fields': ('date_init', 'date_end', 'course_name', 'place',)
            }),
        )
    inlines = [TranslationInline,]

class HobbiesInfoAdmin(TranslatableAdmin):
    list_display = ('title', 'description',)
    list_display_links = ('title',)

    fieldsets = (
            (u"Work Experience", {
                'fields': ('title', 'description',)
            }),
        )
    inlines = [TranslationInline,]

class ContactMeAdmin(admin.ModelAdmin):
    list_display = ('date', 'full_name', 'subject', 'email', 'message', 'close_question',)
    list_editable = ('close_question',)

class VisitorNumberInfoAdmin(SingletonModelAdmin):
    list_display = ('date', 'VisitorNumber',)
    list_editable = ('VisitorNumber',)

class LocalizeIpVisitorAdmin(admin.ModelAdmin):
    list_display = ('date', 'ip', 'country', 'most_specific', 'city', 'postal_code', 'latitude', 'longitude',)
    list_editable = ('ip',)

admin.site.register(Config, ConfigAdmin)
admin.site.register(SkillInfo, SkillInfoAdmin)
admin.site.register(WorkExperienceInfo, WorkExperienceInfoAdmin)
admin.site.register(EducationInfo, EducationInfoAdmin)
admin.site.register(HobbiesInfo, HobbiesInfoAdmin)
admin.site.register(ContactMe, ContactMeAdmin)
admin.site.register(VisitorNumberInfo, VisitorNumberInfoAdmin)
admin.site.register(LocalizeIpVisitor, LocalizeIpVisitorAdmin)