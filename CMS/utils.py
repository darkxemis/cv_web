from django.core.mail import EmailMessage

def send_email(from_user, to_user, subject, message, attach_file):
	message_obj = EmailMessage(subject, message, from_user, [to_user])
	if attach_file:
		message_obj.attach_file('media/main_img.png')

	message_obj.send()


