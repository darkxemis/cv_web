jQuery(function ($) {

    'use strict';

    // --------------------------------------------------------------------
    // PreLoader
    // --------------------------------------------------------------------

    (function () {
        $('#preloader').delay(200).fadeOut('slow');
        $('#submit').css('background-color','grey');

        $(document).ready(function(){
            $(':input[name="submit"]').prop('disabled', true);

            $('input[name="check-privacity"]').click(function(){
                if($(this).prop("checked") == true){
                    $(':input[name="submit"]').prop('disabled', false);
                    $('#submit').css('background-color','#FFC107');
                }
                else if($(this).prop("checked") == false){
                    $(':input[name="submit"]').prop('disabled', true);
                    $('#submit').css('background-color','grey');
                }
            });
        });
    }());



    // --------------------------------------------------------------------
    // Sticky Sidebar
    // --------------------------------------------------------------------

    $('.left-col-block, .right-col-block').theiaStickySidebar();

}); // JQuery end