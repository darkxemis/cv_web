from django.db import models

from translations.models import Translatable

# Create your models here.
class Config(Translatable):
    name = models.CharField(max_length=30, default="", verbose_name='name')
    job_title = models.CharField(max_length=30, default="", verbose_name='Job title')
    photo = models.ImageField(verbose_name=u"Photo CV", upload_to="photo_cv")
    phone_1 = models.CharField(max_length=23, default="", verbose_name='Phone Spain')
    email = models.EmailField(max_length=50, default="", verbose_name='Email')
    address = models.CharField(verbose_name=u'Address', max_length=200)
    description = models.TextField(blank=True, null=True, verbose_name=u'Description')

    favicon = models.ImageField(verbose_name=u"favicon", upload_to="favicon")

    def __str__(self):
        return u"Website configuration"

    class Meta:
        verbose_name = u"Website configuration"
        verbose_name_plural = u"Website configuration"
    
    class TranslatableMeta:
        fields = ['job_title', 'description']

class SkillInfo(Translatable):
    name_skill = models.CharField(max_length=30, default="", blank=False, verbose_name='Name skill')
    percentage_skill = models.PositiveIntegerField()
    code_ico = models.ImageField(verbose_name=u"Code ico", upload_to="code_ico")

    def __str__(self):
        return self.name_skill
    
    def get_percentage_skill(self):
        return str(self.percentage_skill) + "%;"

    class Meta:
        verbose_name = u"Skill info"
        verbose_name_plural = u"Skill info"

    class TranslatableMeta:
        fields = ['name_skill']


class InformationModel(models.Model):
    date_init = models.DateTimeField()
    date_end = models.DateTimeField()

    place = models.CharField(max_length=100, default="", blank=False, verbose_name='Place')

    class Meta:         
        abstract = True

class WorkExperienceInfo(InformationModel, Translatable):
    name = models.CharField(max_length=100, default="", blank=False, verbose_name='Name')
    job = models.CharField(max_length=100, default="", blank=False, verbose_name='Job name')

    def __str__(self):
        return self.name + " " + self.job + " " + self.place

    class Meta:
        verbose_name = u"Work experience"
        verbose_name_plural = u"Work experience"

    class TranslatableMeta:
        fields = ['job', 'place']

class EducationInfo(InformationModel, Translatable):
    course_name = models.CharField(max_length=100, default="", blank=False, verbose_name='Course name')

    def __str__(self):
        return self.course_name + " " + self.place

    class Meta:
        verbose_name = u"Education"
        verbose_name_plural = u"Education"

    class TranslatableMeta:
        fields = ['course_name', 'place']

class HobbiesInfo(Translatable):
    title = models.CharField(max_length=50, default="", blank=False, verbose_name='Title')
    description = models.TextField(blank=False, verbose_name=u'Description')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = u"Hobbies"
        verbose_name_plural = u"Hobbies"

    class TranslatableMeta:
        fields = ['title', 'description']

class ContactMe(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=False)
    full_name = models.CharField(max_length=100, blank=False)
    subject = models.CharField(max_length=100, blank=False)
    email = models.EmailField(max_length=127, blank=False, verbose_name='Email contact')
    message = models.TextField(max_length=4048, blank=False)
    close_question = models.BooleanField(blank=False, default=False, verbose_name='Close question')

    def __str__(self):
        return self.full_name
    
    class Meta:
        verbose_name = u"Contact me"
        verbose_name_plural = u"Contact me"

class VisitorNumberInfo(models.Model):
    date = models.DateTimeField(blank=False, verbose_name=u'Date')
    VisitorNumber = models.PositiveIntegerField(blank=False, verbose_name=u'Visitor number')

    def __str__(self):
        return str(self.date)
    
    class Meta:
        verbose_name = u"Visitor number"
        verbose_name_plural = u"Visitor number"

class LocalizeIpVisitor(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=False, verbose_name=u'Date')
    ip = models.CharField(max_length=50, blank=False, verbose_name=u'IP')
    country = models.CharField(max_length=50, blank=False, verbose_name=u'Country')
    most_specific = models.CharField(max_length=50, blank=False, verbose_name=u'Most specific name')
    city = models.CharField(max_length=50, blank=False, verbose_name=u'City')
    postal_code = models.CharField(max_length=50, blank=False, verbose_name=u'Postal code')
    latitude = models.CharField(max_length=50, blank=False, verbose_name=u'Latitude')
    longitude = models.CharField(max_length=50, blank=False, verbose_name=u'Longitude')

    def __str__(self):
        return str(self.ip)
    
    class Meta:
        verbose_name = u"Localize IP visitor"
        verbose_name_plural = u"Localize IP visitor"



