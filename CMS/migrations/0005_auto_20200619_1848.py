# Generated by Django 3.0.7 on 2020-06-19 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0004_config'),
    ]

    operations = [
        migrations.AlterField(
            model_name='config',
            name='favicon',
            field=models.ImageField(upload_to='favicon', verbose_name='favicon'),
        ),
        migrations.AlterField(
            model_name='config',
            name='photo',
            field=models.ImageField(upload_to='photo_cv', verbose_name='Photo CV'),
        ),
    ]
