from django.contrib import admin
from django.urls import path, include

#Imports need it for media_url
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('admin/', admin.site.urls),
    path('', include('CMS.urls')),
    #path('i18n/', include('django.conf.urls.i18n')),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

