from django.shortcuts import render, redirect
from .models import *
from .utils import *
from .forms import ContactMeForm

from django.utils import translation
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from django.conf import settings

import geoip2.database

# Create your views here.
def index(request):
    contact_form = ContactMeForm()
    
    if 'language' not in request.session:
        language_code = "es" 
        request.session['language'] = language_code
        
        visitor_number_object = VisitorNumberInfo.objects.first()
        visitor_number_object.VisitorNumber = visitor_number_object .VisitorNumber + 1
        visitor_number_object.date = datetime.now()
        visitor_number_object.save()

        #Añadir IP a la base de datos con la información
        '''if visitor_ip_address(request) != "127.0.0.1":
            localize_ip(request, visitor_ip_address(request))'''
            
    else:
        #request.session['language'] = 'es'
        language_code = request.session['language']

    config = Config.objects.probe(['es', 'en']).translate(language_code).first()
    skills = SkillInfo.objects.all().probe(['es', 'en']).translate(language_code)

    work_experience = WorkExperienceInfo.objects.all().probe(['es', 'en']).translate(language_code).order_by('-date_init')
    education = EducationInfo.objects.all().probe(['es', 'en']).translate(language_code)
    hobbies = HobbiesInfo.objects.all().probe(['es', 'en']).translate(language_code)

    return render(request, 'CMS/index.html', locals())

def change_language(request, language_code):
    translation.activate(language_code)
    request.session['language'] = language_code
    #settings.LANGUAGE_CODE = language_code

    return redirect('CMS:index')

#Método para mandar un correo de preguntas del usuario
def send_message(request):
    if request.method == "POST":
        contact_form = ContactMeForm(request.POST)

        if contact_form.is_valid():
            full_name = contact_form.cleaned_data['full_name']
            subject = contact_form.cleaned_data['subject']
            email = contact_form.cleaned_data['email']
            message = contact_form.cleaned_data['message']

            try:
                #subject = "Kontakt: " + from_user + " eller telefon: " + "+" + str(prefix) + " " + str(phone)
                subject = subject + " (Website Email)"
                send_email(email, settings.EMAIL_HOST_USER, subject, message, False)
                contact_form.save()
                return HttpResponseRedirect('/')
            except Exception as e:
                #Filtros de errores hay que poner
                print(e)
                return redirect('CMS:index')
                pass

    return redirect('CMS:index')

def visitor_ip_address(request):

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def localize_ip(request, ip_save):
    reader = geoip2.database.Reader(settings.PATH_BD_IP_LOCALIZE)

    response = reader.city(ip_save)

    localize_ip_obj = LocalizeIpVisitor(ip=ip_save, country=response.country.name, most_specific=response.subdivisions.most_specific.name, 
    city=response.city.name, postal_code=response.postal.code, latitude=response.location.latitude, longitude=response.location.longitude)

    localize_ip_obj.save()

    reader.close()

def load_privacity(request):
    return render(request, 'CMS/political_privacity_' + request.session['language'] +  '.html', locals())
